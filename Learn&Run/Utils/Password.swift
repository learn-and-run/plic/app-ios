//
//  Password.swift
//  Learn&Run
//
//  Created by Antonin on 01/05/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import Foundation

class Password {
    
    static func validatePassword(enteredPassword: String) -> Bool {
        let passwordFormat = "^(?=.*\\d)(?=(.*\\W))(?=.*[a-zA-Z])(?!.*\\s).{5,72}$"
        let passwordPredicate = NSPredicate(format:"SELF MATCHES %@", passwordFormat)
        return passwordPredicate.evaluate(with: enteredPassword)
    }
}
