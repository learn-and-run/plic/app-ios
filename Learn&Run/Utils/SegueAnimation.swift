//
//  SegueAnimation.swift
//  Learn&Run
//
//  Created by Antonin on 13/04/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class CustomSegue: UIStoryboardSegue {
    
    var duration: Double = 0.5
    var delay: Double = 0.0
    
    func perform(x: CGFloat, y: CGFloat) {
        let src = self.source
        let dst = self.destination

        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        dst.view.transform = CGAffineTransform(translationX: x, y: y)

        UIView.animate(withDuration: self.duration,
                       delay: self.delay,
                       options: .curveEaseInOut,
                       animations: {dst.view.transform = CGAffineTransform(translationX: 0, y: 0)},
                       completion: { finished in src.present(dst, animated: false, completion: nil)})
    }
}

@IBDesignable
class RightToLeftSegue: CustomSegue {
    
    override func perform() {
        perform(x: self.source.view.frame.size.width, y: 0)
    }
}

@IBDesignable
class LeftToRightSegue: CustomSegue {
    
    override func perform() {
        perform(x: -self.source.view.frame.size.width, y: 0)
    }
}

@IBDesignable
class UpToBottomSegue: CustomSegue {
    
    override func perform() {
        perform(x: 0, y: -self.source.view.frame.size.height)
    }
}

@IBDesignable
class BottomToUpSegue: CustomSegue {
    
    override func perform() {
        perform(x: 0, y: self.source.view.frame.size.height)
    }
}
