//
//  CustomAlert.swift
//  Learn&Run
//
//  Created by Antonin on 27/10/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    func addCustomAction(_ action: UIAlertAction) {
        if (action.style == .destructive) {
            action.setValue(UIColor(named: "RedColor"), forKey: "titleTextColor")
        }
        else {
            action.setValue(UIColor(named: "GreenColor"), forKey: "titleTextColor")
        }
        self.addAction(action)
    }
    
    func setCustomStyle() {
        setBackgroundColor(color: UIColor(named: "BeigeColor")!)
        setTitle(font: UIFont(name: "Chilanka-Regular", size: 22), color: UIColor(named: "GreenColor"))
        setMessage(font: UIFont(name: "Chilanka-Regular", size: 14), color: UIColor(named: "GreenColor"))
    }
    
    // Set background color of UIAlertController
    private func setBackgroundColor(color: UIColor) {
        if let bgView = self.view.subviews.first, let groupView = bgView.subviews.first, let contentView = groupView.subviews.first {
            contentView.backgroundColor = color
        }
    }
    
    // Set title font and title color
    private func setTitle(font: UIFont?, color: UIColor?) {
        guard let title = self.title else { return }
        
        let attributeString = NSMutableAttributedString(string: title)
        if let titleFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : titleFont], range: NSMakeRange(0, title.count))
        }
        
        if let titleColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor], range: NSMakeRange(0, title.count))
        }
        
        self.setValue(attributeString, forKey: "attributedTitle")
    }
    
    // Set message font and message color
    private func setMessage(font: UIFont?, color: UIColor?) {
        guard let message = self.message else { return }
        
        let attributeString = NSMutableAttributedString(string: message)
        if let messageFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : messageFont], range: NSMakeRange(0, message.count))
        }
        
        if let messageColorColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : messageColorColor], range: NSMakeRange(0, message.count))
        }
        
        self.setValue(attributeString, forKey: "attributedMessage")
    }
}
