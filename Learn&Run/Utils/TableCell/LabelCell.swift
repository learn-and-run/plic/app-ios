//
//  LabelCell.swift
//  Learn&Run
//
//  Created by Antonin on 13/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class LabelCell: UITableViewCell {

    @IBOutlet weak var uiLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
