//
//  ScoreCircuitCell.swift
//  Learn&Run
//
//  Created by Antonin on 14/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class ScoreCircuitCell: UITableViewCell {

    @IBOutlet weak var uiBullet: UIImageView!
    @IBOutlet weak var uiCircuitName: UILabel!
    @IBOutlet weak var uiScore: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
