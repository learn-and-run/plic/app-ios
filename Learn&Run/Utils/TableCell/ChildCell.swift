//
//  ChildCell.swift
//  Learn&Run
//
//  Created by Antonin on 03/07/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class ChildCell: UITableViewCell {
    
    
    @IBOutlet weak var uiChildName: UILabel!
    @IBOutlet weak var uiChildClass: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
