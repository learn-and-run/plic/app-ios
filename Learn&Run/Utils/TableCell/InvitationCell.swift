//
//  InvitationCell.swift
//  Learn&Run
//
//  Created by Antonin on 04/10/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class InvitationCell: UITableViewCell {
    
    @IBOutlet weak var uiUsername: UILabel!
    @IBOutlet weak var uiCancelInvitation: UIImageView!
    @IBOutlet weak var uiAcceptInvitation: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
