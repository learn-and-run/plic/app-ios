//
//  Json.swift
//  Learn&Run
//
//  Created by Antonin on 13/04/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import Foundation

extension JSONDecoder {
    
    func decodeError<T>(_ type: T.Type, from: Data) throws -> T where T : Decodable {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        self.dateDecodingStrategy = .formatted(dateFormatter)
        return try self.decode(type, from: from)
    }
    
    func decodeData<T>(_ type: T.Type, from: Data) throws -> T where T : Decodable {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        self.dateDecodingStrategy = .formatted(dateFormatter)
        return try self.decode(type, from: from)
    }
}
