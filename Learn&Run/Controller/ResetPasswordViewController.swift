//
//  ResetPasswordViewController.swift
//  Learn&Run
//
//  Created by Antonin on 14/04/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class ResetPasswordViewController: AbstractController, FreezeProtocol, UITextFieldDelegate {

    @IBOutlet weak var uiEmailField: UITextField!
    @IBOutlet weak var uiSendButton: UIButton!
    @IBOutlet weak var uiLoadingAnimation: UIActivityIndicatorView!
    
    @IBAction func onEditingEmail() {
        if uiEmailField.text?.isEmpty ?? true {
            disableSendButton()
        }
        else {
            enableSendButton()
        }
    }
    
    private func disableSendButton() {
        uiSendButton.isUserInteractionEnabled = false
        let color: UIColor = UIColor(named: "LightGrayColor")!
        uiSendButton.backgroundColor = color
        uiSendButton.borderColor = color
    }
    
    private func enableSendButton() {
        uiSendButton.isUserInteractionEnabled = true
        let color: UIColor = UIColor(named: "BeigeColor")!
        uiSendButton.backgroundColor = color
        uiSendButton.borderColor = color
    }
    
    @IBAction func onClickSend() {
        startFreezeScreen()
        
        if !Email.validateEmail(enteredEmail: uiEmailField.text!) {
            handleError(message: "Email incorrect !")
            return
        }
        
        HttpCall.shared.askResetPassword(email: uiEmailField.text!).call {
            self.uiLoadingAnimation.stopAnimating()
            self.displayAlert(title: "Changement de mot de passe", message: "Un email vient d'être envoyé sur votre boîte mail !") { (_) in
                self.performSegue(withIdentifier: "ResetToLoginSegue", sender: self)
            }
        } onError: { (_) in
            self.handleError(message: "Une erreur est survenue !")
        }
    }

    private func handleError(message: String) {
        cancelFreezeScreen()
        displayAlert(title: "Erreur", message: message)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !(uiEmailField.text?.isEmpty ?? true) {
            textField.endEditing(true)
            onClickSend()
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uiEmailField.delegate = self
    }
}
