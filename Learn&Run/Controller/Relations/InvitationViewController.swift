//
//  InvitationViewController.swift
//  Learn&Run
//
//  Created by Antonin on 04/10/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class InvitationViewController: AbstractController, UITableViewDelegate, UITableViewDataSource {

    var callBackDismiss: (() -> Void)? = nil
    
    private var invitations: [PendingInvitationsDto.PendingInvitationDto] = []
    
    @IBOutlet weak var uiBackArrow: UIImageView!
    @IBOutlet weak var uiTableInvitation: UITableView!
    
    private func disappearInvitation(id: Int) {
        invitations.remove(at: id)
        uiTableInvitation.reloadData()
    }
    
    @objc private func onClickCancel(_ sender: UITapGestureRecognizer) {
        let index = sender.view!.tag
        let invitation = invitations[index]
        HttpCall.shared.declineInvitation(id: invitation.id).call()
        disappearInvitation(id: index)
    }
    
    @objc private func onClickAccept(_ sender: UITapGestureRecognizer) {
        let index = sender.view!.tag
        let invitation = invitations[index]
        HttpCall.shared.acceptInvitation(id: invitation.id).call()
        disappearInvitation(id: index)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        invitations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = uiTableInvitation.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! InvitationCell
        cell.layoutMargins = UIEdgeInsets.zero
        
        let invitation = invitations[indexPath.row]
        cell.uiUsername.text = invitation.user.pseudo
        cell.uiCancelInvitation.tag = indexPath.row
        cell.uiCancelInvitation.isUserInteractionEnabled = true
        cell.uiAcceptInvitation.tag = indexPath.row
        cell.uiAcceptInvitation.isUserInteractionEnabled = true
        let onClickCancel = UITapGestureRecognizer(target: self, action: #selector(onClickCancel(_:)))
        cell.uiCancelInvitation.addGestureRecognizer(onClickCancel)
        let onClickAccept = UITapGestureRecognizer(target: self, action: #selector(onClickAccept(_:)))
        cell.uiAcceptInvitation.addGestureRecognizer(onClickAccept)
        return cell
    }
    
    @objc private func onClickBackArrow(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        callBackDismiss?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uiTableInvitation.delegate = self
        uiTableInvitation.dataSource = self

        let onClickBackArrow = UITapGestureRecognizer(target: self, action: #selector(onClickBackArrow(_:)))
        self.uiBackArrow.addGestureRecognizer(onClickBackArrow)
        
        uiTableInvitation.layoutMargins = UIEdgeInsets.zero
        uiTableInvitation.separatorInset = UIEdgeInsets.zero
        uiTableInvitation.rowHeight = 100
        
        HttpCall.shared.getPendingInvitations().call() { (pendingInvitationsDto: PendingInvitationsDto) in
            self.invitations = pendingInvitationsDto.pendingInvitations
            self.uiTableInvitation.reloadData()
        } onError: { (_) in
            self.dismiss(animated: true, completion: nil)
        }
    }
}
