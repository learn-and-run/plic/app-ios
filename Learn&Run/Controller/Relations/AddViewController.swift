//
//  AddViewController.swift
//  Learn&Run
//
//  Created by Antonin on 03/10/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class AddViewController: AbstractController, FreezeProtocol, UITextFieldDelegate {
    
    var label: String = ""
    
    @IBOutlet weak var uiLabel: UILabel!
    @IBOutlet weak var uiArrowBack: UIImageView!
    @IBOutlet weak var uiUsername: UITextField!
    @IBOutlet weak var uiLoadingAnimation: UIActivityIndicatorView!
    @IBOutlet weak var uiValidateButton: UIButton!
    
    private func disableValidateButton() {
        uiValidateButton.isUserInteractionEnabled = false
        let color: UIColor = UIColor(named: "LightGrayColor")!
        uiValidateButton.backgroundColor = color
        uiValidateButton.borderColor = color
    }

    private func enableValidateButton() {
        uiValidateButton.isUserInteractionEnabled = true
        let color: UIColor = UIColor(named: "BeigeColor")!
        uiValidateButton.backgroundColor = color
        uiValidateButton.borderColor = color
    }
    
    private func handleError(message: String) {
        disableValidateButton()
        cancelFreezeScreen()
        displayAlert(title: "Erreur", message: message)
    }
    
    @IBAction func onChangeUsername(_ sender: Any) {
        if let username = uiUsername.text, !username.isEmpty {
            enableValidateButton()
        }
        else {
            disableValidateButton()
        }
    }
    
    @IBAction func onClickValidate() {
        startFreezeScreen()

        HttpCall.shared.sendInvitation(username: uiUsername.text!).call {
            self.dismiss(animated: true, completion: nil)
        } onError: { (_) in
            self.handleError(message: "Une erreur est survenue !")
        }
    }
    
    @objc private func onClickArrowBack(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !(uiUsername.text?.isEmpty ?? true) {
            onClickValidate()
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let onClickArrowBack = UITapGestureRecognizer(target: self, action: #selector(self.onClickArrowBack(_:)))
        self.uiArrowBack.addGestureRecognizer(onClickArrowBack)
        
        uiUsername.delegate = self
        
        uiLabel.text = label
    }
}
