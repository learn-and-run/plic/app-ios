//
//  ParentsViewController.swift
//  Learn&Run
//
//  Created by Antonin on 03/10/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class ParentsViewController: AbstractController, UITableViewDelegate, UITableViewDataSource {

    private var parentsList: [RelationsDto.RelationDto] = []
    
    @IBOutlet weak var uiBackArrow: UIImageView!
    @IBOutlet weak var uiTableParents: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        parentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = uiTableParents.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChildCell
        cell.layoutMargins = UIEdgeInsets.zero
        
        let child = parentsList[indexPath.row].user as! RelationsDto.RelationDto.ParentDto
        cell.uiChildName.text = child.pseudo
        cell.uiChildClass.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "Êtes-vous sûr de vouloir supprimer cette relation ?", message: "Cette action est définitive !", preferredStyle: .alert)
            
            alert.addCustomAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
            alert.addCustomAction(UIAlertAction(title: "OK", style: .destructive, handler: { (_) in
                HttpCall.shared.deleteRelation(id: self.parentsList[indexPath.row].id).call()
                self.parentsList.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .left)
            }))
            
            alert.setCustomStyle()
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClickAddFriend() {
        self.performSegue(withIdentifier: "ParentsToAddSegue", sender: self)
    }
    
    @objc private func onClickArrowBack(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ParentsToAddSegue" {
            (segue.destination as! AddViewController).label = "Ajoute un parent !"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uiTableParents.delegate = self
        uiTableParents.dataSource = self
        
        let onClickArrowBack = UITapGestureRecognizer(target: self, action: #selector(self.onClickArrowBack(_:)))
        self.uiBackArrow.addGestureRecognizer(onClickArrowBack)
        
        uiTableParents.layoutMargins = UIEdgeInsets.zero
        uiTableParents.separatorInset = UIEdgeInsets.zero
        uiTableParents.rowHeight = 100
        
        HttpCall.shared.getAllParents().call() { (relationsDto: RelationsDto) in
            self.parentsList = relationsDto.relations
            self.uiTableParents.reloadData()
        } onError: { (_) in
            self.dismiss(animated: true, completion: nil)
        }
    }
}
