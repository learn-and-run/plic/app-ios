//
//  FriendsViewController.swift
//  Learn&Run
//
//  Created by Antonin on 03/10/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class FriendsViewController: AbstractController, UITableViewDelegate, UITableViewDataSource {
    
    private var friendsList: [RelationsDto.RelationDto] = []

    @IBOutlet weak var uiBackArrow: UIImageView!
    @IBOutlet weak var uiTableFriends: UITableView!
    @IBOutlet weak var uiRank: UIImageView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        friendsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = uiTableFriends.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChildCell
        cell.layoutMargins = UIEdgeInsets.zero
        
        let child = friendsList[indexPath.row].user as! RelationsDto.RelationDto.StudentDto
        cell.uiChildName.text = child.pseudo
        cell.uiChildClass.text = child.levelType.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "FriendsToStatsSegue", sender: friendsList[indexPath.row].user.pseudo)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "Êtes-vous sûr de vouloir supprimer cette relation ?", message: "Cette action est définitive !", preferredStyle: .alert)
            
            alert.addCustomAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
            alert.addCustomAction(UIAlertAction(title: "OK", style: .destructive, handler: { (_) in
                HttpCall.shared.deleteRelation(id: self.friendsList[indexPath.row].id).call()
                self.friendsList.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .left)
            }))
            
            alert.setCustomStyle()
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClickAddFriend() {
        self.performSegue(withIdentifier: "FriendsToAddSegue", sender: self)
    }
    
    @objc private func onClickArrowBack(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func onClickRank(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "FriendsToRankSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FriendsToAddSegue" {
            (segue.destination as! AddViewController).label = "Ajoute un ami !"
        }
        else if segue.identifier == "FriendsToStatsSegue" {
            (segue.destination as! StatisticsChildController).pseudo = sender as? String
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uiTableFriends.delegate = self
        uiTableFriends.dataSource = self
        
        let onClickArrowBack = UITapGestureRecognizer(target: self, action: #selector(self.onClickArrowBack(_:)))
        self.uiBackArrow.addGestureRecognizer(onClickArrowBack)
        let onClickRank = UITapGestureRecognizer(target: self, action: #selector(self.onClickRank(_:)))
        self.uiRank.addGestureRecognizer(onClickRank)
        
        uiTableFriends.layoutMargins = UIEdgeInsets.zero
        uiTableFriends.separatorInset = UIEdgeInsets.zero
        uiTableFriends.rowHeight = 100
        
        HttpCall.shared.getAllStudents().call() { (relationsDto: RelationsDto) in
            self.friendsList = relationsDto.relations
            self.uiTableFriends.reloadData()
        } onError: { (_) in
            self.dismiss(animated: true, completion: nil)
        }
    }
}
