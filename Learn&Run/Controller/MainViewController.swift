//
//  MainViewController.swift
//  Learn&Run
//
//  Created by Antonin on 13/04/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class MainViewController: AbstractController {

    @IBOutlet weak var uiImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        uiImageView.image = UIImage.gif(asset: "AnimatedLogo")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        HttpCall.shared.isAuthenticated().call { (isAuthenticatedDto: IsAuthenticatedDto) in
            if isAuthenticatedDto.isAuthenticated {
                self.goToMain()
            }
            else {
                self.goToLogin()
            }
        } onError: { (_) in
            self.goToLogin()
        }
        
        sleep(2)
    }
    
    private func goToLogin() {
        self.performSegue(withIdentifier: "MainToLoginSegue", sender: self)
    }
    
    private func goToMain() {
        self.performSegue(withIdentifier: "MainToRedirectSegue", sender: self)
    }
}
