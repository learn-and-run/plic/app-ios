//
//  NewAccountStudentViewController.swift
//  Learn&Run
//
//  Created by Antonin on 14/04/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class NewAccountStudentViewController: AbstractController, FreezeProtocol, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    private var currentClass: LevelType!
    
    @IBOutlet weak var uiNameField: UITextField!
    @IBOutlet weak var uiFirstnameField: UITextField!
    @IBOutlet weak var uiPseudoField: UITextField!
    @IBOutlet weak var uiClassView: UIView!
    @IBOutlet weak var uiClassField: UITextField!
    @IBOutlet weak var uiClassArrow: UIImageView!
    @IBOutlet weak var uiClassTable: UITableView!
    @IBOutlet var uiClassTableHeight: NSLayoutConstraint!
    @IBOutlet weak var uiEmailField: UITextField!
    @IBOutlet weak var uiPasswordField: UITextField!
    @IBOutlet weak var uiCreateButton: UIButton!
    @IBOutlet weak var uiLoadingAnimation: UIActivityIndicatorView!
    
    @IBAction func onClickEyeButton(_ sender: UIButton) {
        if uiPasswordField.isSecureTextEntry {
            sender.setImage(UIImage(named: "OpenEyeWhite.png"), for: .normal)
            uiPasswordField.isSecureTextEntry = false
        }
        else {
            sender.setImage(UIImage(named: "CloseEyeWhite.png"), for: .normal)
            uiPasswordField.isSecureTextEntry = true
        }
    }
    
    @IBAction func onEditField() {
        if allFieldsFilled() {
            enableCreateButton()
        }
        else {
            disableCreateButton()
        }
    }
    
    @IBAction func onClickCreate() {
        startFreezeScreen()
        
        if !Email.validateEmail(enteredEmail: uiEmailField.text!) {
            handleError(message: "Votre email doit être valide !")
            return
        }

        if !Password.validatePassword(enteredPassword: uiPasswordField.text!) {
            handleError(message: "Le mot de passe doit contenir entre 5 et 72 caractères et être composé d'au moins une minuscule, une majuscule, un chiffre et un caractère spécial !")
            return
        }
        
        HttpCall.shared.createStudent(pseudo: uiPseudoField.text!, firstname: uiFirstnameField.text!, lastname: uiNameField.text!, password: uiPasswordField.text!, email: uiEmailField.text!, levelType: currentClass).call {
            self.uiLoadingAnimation.stopAnimating()
            self.displayAlert(title: "Création de compte", message: "Votre compte a bien été crée, regardez votre boîte mail !") { (_) in
                self.performSegue(withIdentifier: "CreationStudentToLoginSegue", sender: self)
            }
        } onError: { (_) in
            self.handleError(message: "Une erreur est survenue !")
        }
    }
    
    private func disableCreateButton() {
        uiCreateButton.isUserInteractionEnabled = false
        let color: UIColor = UIColor(named: "LightGrayColor")!
        uiCreateButton.backgroundColor = color
        uiCreateButton.borderColor = color
    }
    
    private func enableCreateButton() {
        uiCreateButton.isUserInteractionEnabled = true
        let color: UIColor = UIColor(named: "BeigeColor")!
        uiCreateButton.backgroundColor = color
        uiCreateButton.borderColor = color
    }
    
    private func handleError(message: String) {
        self.cancelFreezeScreen()
        self.displayAlert(title: "Erreur", message: message)
    }
    
    private func allFieldsFilled() -> Bool {
        allFieldsFilledExceptPassword() && !(uiPasswordField.text?.isEmpty ?? true)
    }
    
    private func allFieldsFilledExceptPassword() -> Bool {
        !(uiNameField.text?.isEmpty ?? true ||
        uiFirstnameField.text?.isEmpty ?? true ||
        uiPseudoField.text?.isEmpty ?? true ||
        uiClassField.text?.isEmpty ?? true ||
        uiEmailField.text?.isEmpty ?? true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        hideDropDown()
        
        if textField.accessibilityIdentifier == "password" {
            if allFieldsFilledExceptPassword() {
                textField.returnKeyType = .send
            }
            else {
                textField.returnKeyType = .next
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.accessibilityIdentifier {
        case "name":
            uiFirstnameField.becomeFirstResponder()
        case "firstname":
            uiPseudoField.becomeFirstResponder()
        case "pseudo":
            openDropDown()
        case "email":
            uiPasswordField.becomeFirstResponder()
        default:
            if allFieldsFilled() {
                textField.endEditing(true)
                onClickCreate()
            }
            else {
                uiNameField.becomeFirstResponder()
            }
        }
        return false
    }
    
    @objc private func onClickClass(_ sender: UITapGestureRecognizer) {
        if uiClassTable.isHidden {
            openDropDown()
        }
        else {
            hideDropDown()
        }
    }
    
    @objc private func onClickOutside(_ sender: UITapGestureRecognizer) {
        hideDropDown()
    }
    
    private func openDropDown() {
        view.endEditing(true)
        
        UIView.animate(withDuration: 0.3) {
            self.uiClassTableHeight.isActive = false
            
            self.uiClassView.backgroundColor = UIColor(named: "BeigeColor")
            self.uiClassView.borderColor = UIColor(named: "BeigeColor")
            self.uiClassField.textColor = UIColor(named: "GreenColor")
            self.uiClassField.placeHolderColor = UIColor(named: "GreenColor")
            self.uiClassArrow.tintColor = UIColor(named: "GreenColor")
            
            self.uiClassArrow.transform = CGAffineTransform(rotationAngle: .pi)
            self.view.layoutIfNeeded()
            self.uiClassTable.isHidden = false
        }
    }
    
    private func hideDropDown() {
        UIView.animate(withDuration: 0.3, animations: {
            self.uiClassTableHeight.isActive = true
            
            self.uiClassView.backgroundColor = nil
            self.uiClassView.borderColor = UIColor(named: "WhiteColor")
            self.uiClassField.textColor = UIColor(named: "WhiteColor")
            self.uiClassField.placeHolderColor = UIColor(named: "WhiteColor")
            self.uiClassArrow.tintColor = UIColor(named: "WhiteColor")
            
            self.uiClassArrow.transform = CGAffineTransform(rotationAngle: 0)
            self.view.layoutIfNeeded()
        }) { (_) in
            self.uiClassTable.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        LevelType.allValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = uiClassTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LabelCell
        cell.layoutMargins = UIEdgeInsets.zero
        cell.uiLabel.text = LevelType.allValues[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentClass = LevelType.allValues[indexPath.row]
        uiClassField.text = currentClass.name
        hideDropDown()
        onEditField()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool { !(touch.view?.isDescendant(of: uiClassTable) ?? false) }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uiNameField.delegate = self
        uiFirstnameField.delegate = self
        uiPseudoField.delegate = self
        uiClassTable.delegate = self
        uiClassTable.dataSource = self
        uiEmailField.delegate = self
        uiPasswordField.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onClickOutside(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        let onClickClass = UITapGestureRecognizer(target: self, action: #selector(self.onClickClass(_:)))
        self.uiClassView.addGestureRecognizer(onClickClass)
        
        uiClassTable.layoutMargins = UIEdgeInsets.zero
        uiClassTable.separatorInset = UIEdgeInsets.zero
    }
}
