//
//  NewAccountParentViewController.swift
//  Learn&Run
//
//  Created by Antonin on 14/04/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class NewAccountParentViewController: AbstractController, FreezeProtocol, UITextFieldDelegate {

    @IBOutlet weak var uiNameField: UITextField!
    @IBOutlet weak var uiFirstnameField: UITextField!
    @IBOutlet weak var uiPseudoField: UITextField!
    @IBOutlet weak var uiEmailField: UITextField!
    @IBOutlet weak var uiPasswordField: UITextField!
    @IBOutlet weak var uiCreateButton: UIButton!
    @IBOutlet weak var uiLoadingAnimation: UIActivityIndicatorView!
    
    @IBAction func onClickEyeButton(_ sender: UIButton) {
        if uiPasswordField.isSecureTextEntry {
            sender.setImage(UIImage(named: "OpenEyeWhite.png"), for: .normal)
            uiPasswordField.isSecureTextEntry = false
        }
        else {
            sender.setImage(UIImage(named: "CloseEyeWhite.png"), for: .normal)
            uiPasswordField.isSecureTextEntry = true
        }
    }
    
    @IBAction func onEditField() {
        if allFieldsFilled() {
            enableCreateButton()
        }
        else {
            disableCreateButton()
        }
    }
    
    @IBAction func onClickCreate() {
        startFreezeScreen()
        
        if !Email.validateEmail(enteredEmail: uiEmailField.text!) {
            handleError(message: "Votre email doit être valide !")
            return
        }

        if !Password.validatePassword(enteredPassword: uiPasswordField.text!) {
            handleError(message: "Le mot de passe doit contenir entre 5 et 72 caractères et être composé d'au moins une minuscule, une majuscule, un chiffre et un caractère spécial !")
            return
        }
        
        HttpCall.shared.createParent(pseudo: uiPseudoField.text!, firstname: uiFirstnameField.text!, lastname: uiNameField.text!, password: uiPasswordField.text!, email: uiEmailField.text!).call {
            self.uiLoadingAnimation.stopAnimating()
            self.displayAlert(title: "Création de compte", message: "Votre compte a bien été crée, regardez votre boîte mail !") { (_) in
                self.performSegue(withIdentifier: "CreationParentToLoginSegue", sender: self)
            }
        } onError: { (_) in
            self.handleError(message: "Une erreur est survenue !")
        }
    }
    
    private func disableCreateButton() {
        uiCreateButton.isUserInteractionEnabled = false
        let color: UIColor = UIColor(named: "LightGrayColor")!
        uiCreateButton.backgroundColor = color
        uiCreateButton.borderColor = color
    }
    
    private func enableCreateButton() {
        uiCreateButton.isUserInteractionEnabled = true
        let color: UIColor = UIColor(named: "BeigeColor")!
        uiCreateButton.backgroundColor = color
        uiCreateButton.borderColor = color
    }
    
    private func handleError(message: String) {
        self.cancelFreezeScreen()
        self.displayAlert(title: "Erreur", message: message)
    }

    private func allFieldsFilled() -> Bool {
        allFieldsFilledExceptPassword() && !(uiPasswordField.text?.isEmpty ?? true)
    }
    
    private func allFieldsFilledExceptPassword() -> Bool {
        !(uiNameField.text?.isEmpty ?? true ||
        uiFirstnameField.text?.isEmpty ?? true ||
        uiPseudoField.text?.isEmpty ?? true ||
        uiEmailField.text?.isEmpty ?? true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.accessibilityIdentifier == "password" {
            if allFieldsFilledExceptPassword() {
                textField.returnKeyType = .send
            }
            else {
                textField.returnKeyType = .next
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.accessibilityIdentifier {
        case "name":
            uiFirstnameField.becomeFirstResponder()
        case "firstname":
            uiPseudoField.becomeFirstResponder()
        case "pseudo":
            uiEmailField.becomeFirstResponder()
        case "email":
            uiPasswordField.becomeFirstResponder()
        default:
            if allFieldsFilled() {
                textField.endEditing(true)
                onClickCreate()
            }
            else {
                uiNameField.becomeFirstResponder()
            }
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uiNameField.delegate = self
        uiFirstnameField.delegate = self
        uiPseudoField.delegate = self
        uiEmailField.delegate = self
        uiPasswordField.delegate = self
    }
}
