//
//  HomeRedirectionViewController.swift
//  Learn&Run
//
//  Created by Antonin on 14/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class HomeRedirectionViewController: AbstractController {
    
    private var pseudo: String?
    
    private func goToLogin() {
        performSegue(withIdentifier: "RedirectToLoginSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RedirectToHomeStudentSegue" {
            HomeStudentViewController.pseudo = pseudo!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        HttpCall.shared.getUserInfo().call { (userInfoDto: UserInfoDto) in
            switch userInfoDto.userType {
            case .STUDENT:
                self.pseudo = userInfoDto.user.pseudo
                self.performSegue(withIdentifier: "RedirectToHomeStudentSegue", sender: self)
            case .PARENT:
                self.performSegue(withIdentifier: "RedirectToHomeParentSegue", sender: self)
            default:
                self.goToLogin()
            }
        } onError: { (_) in
            self.goToLogin()
        }
    }
}
