//
//  HomeParentViewController.swift
//  Learn&Run
//
//  Created by Antonin on 14/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class HomeParentViewController: AbstractController, UITableViewDelegate, UITableViewDataSource {

    private var childrenList: [RelationsDto.RelationDto] = []
    private var pseudo: String?
    
    @IBOutlet weak var uiInvitation: UIImageView!
    @IBOutlet weak var uiSettings: UIImageView!
    @IBOutlet weak var uiChildrenTable: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        childrenList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = uiChildrenTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChildCell
        cell.layoutMargins = UIEdgeInsets.zero
        
        let child = childrenList[indexPath.row].user as! RelationsDto.RelationDto.StudentDto
        cell.uiChildName.text = child.pseudo
        cell.uiChildClass.text = child.levelType.name
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pseudo = childrenList[indexPath.row].user.pseudo
        self.performSegue(withIdentifier: "HomeParentToStatisticsChildSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "Êtes-vous sûr de vouloir supprimer cette relation ?", message: "Cette action est définitive !", preferredStyle: .alert)
            
            alert.addCustomAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
            alert.addCustomAction(UIAlertAction(title: "OK", style: .destructive, handler: { (_) in
                HttpCall.shared.deleteRelation(id: self.childrenList[indexPath.row].id).call()
                self.childrenList.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .left)
            }))
            
            alert.setCustomStyle()
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func loadChildren() {
        HttpCall.shared.getAllStudents().call { (relationsDto: RelationsDto) in
            self.childrenList = relationsDto.relations
            self.uiChildrenTable.reloadData()
        } onError: { (_) in
            self.performSegue(withIdentifier: "HomeParentToLoginSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeParentToStatisticsChildSegue" {
            let viewController = segue.destination as! StatisticsChildController;
            viewController.pseudo = pseudo!
        }
        else if segue.identifier == "HomeParentToInvitationSegue" {
            (segue.destination as! InvitationViewController).callBackDismiss = {
                self.loadChildren()
            }
        }
        else if segue.identifier == "HomeParentToAddSegue" {
            (segue.destination as! AddViewController).label = "Ajoute un enfant !"
        }
    }
    
    @objc private func onClickInvitations(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "HomeParentToInvitationSegue", sender: self)
    }
    
    @objc private func onClickSettings(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "HomeParentToParametersSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uiChildrenTable.delegate = self
        uiChildrenTable.dataSource = self
        
        let onClickInvitations = UITapGestureRecognizer(target: self, action: #selector(onClickInvitations(_:)))
        self.uiInvitation.addGestureRecognizer(onClickInvitations)
        let onClickSettings = UITapGestureRecognizer(target: self, action: #selector(onClickSettings(_:)))
        uiSettings.addGestureRecognizer(onClickSettings)
        
        uiChildrenTable.layoutMargins = UIEdgeInsets.zero
        uiChildrenTable.separatorInset = UIEdgeInsets.zero
        uiChildrenTable.rowHeight = 100
        
        loadChildren()
    }
}
