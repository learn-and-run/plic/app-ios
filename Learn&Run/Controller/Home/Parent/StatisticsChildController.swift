//
//  StatisticsChildController.swift
//  Learn&Run
//
//  Created by Antonin on 06/07/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class StatisticsChildController: AbstractController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    var pseudo: String!
    var moduleType: ModuleType = .GENERAL
    var circuitScores: [ScoresDto.ScoreDto] = []
    
    @IBOutlet weak var uiArrowBack: UIImageView!
    @IBOutlet weak var uiChildName: UILabel!
    @IBOutlet weak var uiDropDown: UIView!
    @IBOutlet weak var uiModuleTypeLabel: UILabel!
    @IBOutlet weak var uiArrow: UIImageView!
    @IBOutlet weak var uiModuleTable: UITableView!
    @IBOutlet var uiModuleTableHeight: NSLayoutConstraint!
    @IBOutlet weak var uiScoreTable: UITableView!
    
    private func openDropDown() {
        uiModuleTableHeight.isActive = false
        
        UIView.animate(withDuration: 0.3) {
            self.uiArrow.transform = CGAffineTransform(rotationAngle: .pi)
            self.view.layoutIfNeeded()
            self.uiModuleTable.isHidden = false
        }
    }
    
    private func hideDropDown() {
        uiModuleTableHeight.isActive = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.uiArrow.transform = CGAffineTransform(rotationAngle: 0)
            self.view.layoutIfNeeded()
        }) { (_) in
            self.uiModuleTable.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == uiModuleTable {
            return ModuleType.allValues.count
        }
        else {
            return circuitScores.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == uiModuleTable {
            let cell = uiModuleTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LabelCell
            cell.layoutMargins = UIEdgeInsets.zero
            cell.uiLabel.text = ModuleType.allValues[indexPath.row].name
            cell.backgroundColor = ModuleType.allValues[indexPath.row].color
            return cell
        }
        else {
            let cell = uiScoreTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ScoreCircuitCell
            cell.uiBullet.tintColor = moduleType.color
            cell.uiCircuitName.text = circuitScores[indexPath.row].circuitName
            cell.uiCircuitName.textColor = moduleType.color
            cell.uiScore.text = "\(circuitScores[indexPath.row].score)%"
            cell.uiScore.textColor = moduleType.color
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == uiModuleTable {
            moduleType = ModuleType.allValues[indexPath.row]
            loadData()
            
            uiModuleTypeLabel.text = ModuleType.allValues[indexPath.row].name
            uiDropDown.backgroundColor = ModuleType.allValues[indexPath.row].color
            hideDropDown()
        }
    }
    
    @objc private func onClickOutside(_ sender: UITapGestureRecognizer) {
        hideDropDown()
    }
    
    @objc private func onClickDropDown(_ sender: UITapGestureRecognizer) {
        if uiModuleTable.isHidden {
           openDropDown()
        }
        else {
           hideDropDown()
        }
    }
    
    @objc private func onClickArrowBack(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func loadData() {
        HttpCall.shared.getScoresByPseudoAndModule(pseudo: pseudo, moduleType: moduleType).call { (scoresDto: ScoresDto) in
            self.circuitScores = scoresDto.scores
            self.uiScoreTable.reloadData()
        } onError: { (_) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool { !(touch.view?.isDescendant(of: uiModuleTable) ?? false) }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uiModuleTable.delegate = self
        uiModuleTable.dataSource = self
        uiScoreTable.delegate = self
        uiScoreTable.dataSource = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onClickOutside(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        let onClickDropDown = UITapGestureRecognizer(target: self, action: #selector(self.onClickDropDown(_:)))
        self.uiDropDown.addGestureRecognizer(onClickDropDown)
        let onClickArrowBack = UITapGestureRecognizer(target: self, action: #selector(self.onClickArrowBack(_:)))
        self.uiArrowBack.addGestureRecognizer(onClickArrowBack)
        
        uiModuleTable.layoutMargins = UIEdgeInsets.zero
        uiModuleTable.separatorInset = UIEdgeInsets.zero
        uiScoreTable.separatorInset = UIEdgeInsets.zero
        uiScoreTable.rowHeight = 100
        
        uiChildName.text = pseudo
        loadData()
    }
}
