//
//  HomeStudentViewController.swift
//  Learn&Run
//
//  Created by Antonin on 14/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class HomeStudentViewController: AbstractController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
        
    static var pseudo: String?
    
    var moduleType: ModuleType = .GENERAL
    var circuitScores: [ScoresDto.ScoreDto] = []
    
    @IBOutlet weak var uiParentButton: UIImageView!
    @IBOutlet weak var uiFriendButton: UIImageView!
    @IBOutlet weak var uiInvitationButton: UIImageView!
    @IBOutlet weak var uiSettingButton: UIImageView!
    @IBOutlet weak var uiDropDown: UIView!
    @IBOutlet weak var uiModuleTypeLabel: UILabel!
    @IBOutlet weak var uiArrow: UIImageView!
    @IBOutlet weak var uiModuleTable: UITableView!
    @IBOutlet var uiModuleTableHeight: NSLayoutConstraint!
    @IBOutlet weak var uiScoreTable: UITableView!
    
    private func openDropDown() {
        uiModuleTableHeight.isActive = false
        
        UIView.animate(withDuration: 0.3) {
            self.uiArrow.transform = CGAffineTransform(rotationAngle: .pi)
            self.view.layoutIfNeeded()
            self.uiModuleTable.isHidden = false
        }
    }
    
    private func hideDropDown() {
        uiModuleTableHeight.isActive = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.uiArrow.transform = CGAffineTransform(rotationAngle: 0)
            self.view.layoutIfNeeded()
        }) { (_) in
            self.uiModuleTable.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == uiModuleTable {
            return ModuleType.allValues.count
        }
        else {
            return circuitScores.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == uiModuleTable {
            let cell = uiModuleTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LabelCell
            cell.layoutMargins = UIEdgeInsets.zero
            cell.uiLabel.text = ModuleType.allValues[indexPath.row].name
            cell.backgroundColor = ModuleType.allValues[indexPath.row].color
            return cell
        }
        else {
            let cell = uiScoreTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ScoreCircuitCell
            cell.uiBullet.tintColor = moduleType.color
            cell.uiCircuitName.text = circuitScores[indexPath.row].circuitName
            cell.uiCircuitName.textColor = moduleType.color
            cell.uiScore.text = "\(circuitScores[indexPath.row].score)%"
            cell.uiScore.textColor = moduleType.color
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == uiModuleTable {
            moduleType = ModuleType.allValues[indexPath.row]
            loadData()
            
            uiModuleTypeLabel.text = ModuleType.allValues[indexPath.row].name
            uiDropDown.backgroundColor = ModuleType.allValues[indexPath.row].color
            hideDropDown()
        }
    }
    
    @objc private func onClickOutside(_ sender: UITapGestureRecognizer) {
        hideDropDown()
    }
    
    @objc private func onClickParents(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "HomeStudentToParentsSegue", sender: self)
    }
    
    @objc private func onClickFriends(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "HomeStudentToFriendsSegue", sender: self)
    }
    
    @objc private func onClickInvitations(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "HomeStudentToInvitationSegue", sender: self)
    }
    
    @objc private func onClickSettings(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "HomeStudentToParametersSegue", sender: self)
    }
    
    @objc private func onClickDropDown(_ sender: UITapGestureRecognizer) {
        if uiModuleTable.isHidden {
           openDropDown()
        }
        else {
           hideDropDown()
        }
    }
    
    private func loadData() {
        HttpCall.shared.getScoresByPseudoAndModule(pseudo: HomeStudentViewController.pseudo!, moduleType: moduleType).call { (scoresDto: ScoresDto) in
            self.circuitScores = scoresDto.scores
            self.uiScoreTable.reloadData()
        } onError: { (_) in
            self.performSegue(withIdentifier: "HomeStudentToLoginSegue", sender: self)
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool { !(touch.view?.isDescendant(of: uiModuleTable) ?? false) }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uiModuleTable.delegate = self
        uiModuleTable.dataSource = self
        uiScoreTable.delegate = self
        uiScoreTable.dataSource = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onClickOutside(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        let onClickParents = UITapGestureRecognizer(target: self, action: #selector(self.onClickParents(_:)))
        self.uiParentButton.addGestureRecognizer(onClickParents)
        let onClickFriends = UITapGestureRecognizer(target: self, action: #selector(self.onClickFriends(_:)))
        self.uiFriendButton.addGestureRecognizer(onClickFriends)
        let onClickInvitations = UITapGestureRecognizer(target: self, action: #selector(self.onClickInvitations(_:)))
        self.uiInvitationButton.addGestureRecognizer(onClickInvitations)
        let onClickSettings = UITapGestureRecognizer(target: self, action: #selector(self.onClickSettings(_:)))
        self.uiSettingButton.addGestureRecognizer(onClickSettings)
        let onClickDropDown = UITapGestureRecognizer(target: self, action: #selector(self.onClickDropDown(_:)))
        self.uiDropDown.addGestureRecognizer(onClickDropDown)
        
        uiModuleTable.layoutMargins = UIEdgeInsets.zero
        uiModuleTable.separatorInset = UIEdgeInsets.zero
        uiScoreTable.separatorInset = UIEdgeInsets.zero
        uiScoreTable.rowHeight = 100
        
        loadData()
    }
}
