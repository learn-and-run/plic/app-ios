//
//  LoginViewController.swift
//  Learn&Run
//
//  Created by Antonin on 12/04/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class LoginViewController: AbstractController, FreezeProtocol, UITextFieldDelegate {

    @IBOutlet weak var uiUsernameField: UITextField!
    @IBOutlet weak var uiPasswordField: UITextField!
    @IBOutlet weak var uiConnectionButton: UIButton!
    @IBOutlet weak var uiLoadingAnimation: UIActivityIndicatorView!
    
    @IBAction func onClickEyeButton(_ sender: UIButton) {
        if uiPasswordField.isSecureTextEntry {
            sender.setImage(UIImage(named: "OpenEyeWhite.png"), for: .normal)
            uiPasswordField.isSecureTextEntry = false
        }
        else {
            sender.setImage(UIImage(named: "CloseEyeWhite.png"), for: .normal)
            uiPasswordField.isSecureTextEntry = true
        }
    }
    
    @IBAction func onEditField() {
        if uiUsernameField.text?.isEmpty ?? true || uiPasswordField.text?.isEmpty ?? true {
            disableConnectionButton()
        }
        else {
            enableConnectionButton()
        }
    }
    
    @IBAction func onClickConnexion() {
        startFreezeScreen()

        HttpCall.shared.login(username: uiUsernameField.text!, password: uiPasswordField.text!).call {
            UserDefaults.standard.set(self.uiUsernameField.text!, forKey: "Username")
            self.performSegue(withIdentifier: "LoginToRedirectSegue", sender: self)
        } onError: { (_) in
            self.handleError(message: "Une erreur est survenue !")
        }
    }
    
    private func handleError(message: String) {
        uiPasswordField.text = ""
        disableConnectionButton()
        cancelFreezeScreen()
        displayAlert(title: "Erreur", message: message)
    }

    private func disableConnectionButton() {
        uiConnectionButton.isUserInteractionEnabled = false
        let color: UIColor = UIColor(named: "LightGrayColor")!
        uiConnectionButton.backgroundColor = color
        uiConnectionButton.borderColor = color
    }

    private func enableConnectionButton() {
        uiConnectionButton.isUserInteractionEnabled = true
        let color: UIColor = UIColor(named: "BeigeColor")!
        uiConnectionButton.backgroundColor = color
        uiConnectionButton.borderColor = color
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.accessibilityIdentifier == "password" {
            if uiUsernameField.text?.isEmpty ?? true {
                textField.returnKeyType = .next
            }
            else {
                textField.returnKeyType = .send
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.accessibilityIdentifier == "username" {
            uiPasswordField.becomeFirstResponder()
        }
        else {
            if uiUsernameField.text?.isEmpty ?? true {
                uiUsernameField.becomeFirstResponder()
            }
            else {
                textField.endEditing(true)
                onClickConnexion()
            }
        }
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let username = UserDefaults.standard.string(forKey: "Username") {
            uiUsernameField.text = username
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        uiUsernameField.delegate = self
        uiPasswordField.delegate = self
    }
}
