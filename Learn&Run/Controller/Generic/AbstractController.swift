//
//  AbstractController.swift
//  Learn&Run
//
//  Created by Antonin on 12/04/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class AbstractController: UIViewController {
    
    func displayAlert(title: String, message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addCustomAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        alert.setCustomStyle()
        present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .portrait
        }
        return .all
    }
}
