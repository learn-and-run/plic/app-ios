//
//  FreezeProtocol.swift
//  Learn&Run
//
//  Created by Antonin on 12/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

protocol FreezeProtocol: AbstractController {

    var uiLoadingAnimation: UIActivityIndicatorView! { get set }

    func startFreezeScreen()
    func cancelFreezeScreen()
}

extension FreezeProtocol {

    internal func startFreezeScreen() {
        uiLoadingAnimation.startAnimating()
        view.isUserInteractionEnabled = false
    }

    internal func cancelFreezeScreen() {
        uiLoadingAnimation.stopAnimating()
        view.isUserInteractionEnabled = true
    }
}
