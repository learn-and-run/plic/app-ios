//
//  ChangePasswordViewController.swift
//  Learn&Run
//
//  Created by Antonin on 03/10/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class ChangePasswordViewController: AbstractController, FreezeProtocol, UITextFieldDelegate {
    
    var homeSegueName: String = ""
    var backSegueName: String = ""
    
    @IBOutlet weak var uiPassword: UITextField!
    @IBOutlet weak var uiValidateButton: UIButton!
    @IBOutlet weak var uiLoadingAnimation: UIActivityIndicatorView!
    @IBOutlet weak var uiArrowBack: UIImageView!
    
    private func disableValidateButton() {
        uiValidateButton.isUserInteractionEnabled = false
        let color: UIColor = UIColor(named: "LightGrayColor")!
        uiValidateButton.backgroundColor = color
        uiValidateButton.borderColor = color
    }

    private func enableValidateButton() {
        uiValidateButton.isUserInteractionEnabled = true
        let color: UIColor = UIColor(named: "BeigeColor")!
        uiValidateButton.backgroundColor = color
        uiValidateButton.borderColor = color
    }
    
    private func handleError(message: String) {
        uiPassword.text = ""
        disableValidateButton()
        cancelFreezeScreen()
        displayAlert(title: "Erreur", message: message)
    }
    
    @IBAction func onChangePassword() {
        if let email = uiPassword.text, !email.isEmpty {
            enableValidateButton()
        }
        else {
            disableValidateButton()
        }
    }
    
    @IBAction func onClickValidate() {
        startFreezeScreen()

        HttpCall.shared.updatePassword(password: uiPassword.text!).call {
            HttpCall.shared.login(username: UserDefaults.standard.string(forKey: "Username")!, password: self.uiPassword.text!).call() {
                self.performSegue(withIdentifier: self.homeSegueName, sender: self)
            } onError: { (_) in
                self.handleError(message: "Une erreur est survenue !")
            }
        } onError: { (_) in
            self.handleError(message: "Une erreur est survenue !")
        }
    }
    
    @objc private func onClickArrowBack(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: backSegueName, sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !(uiPassword.text?.isEmpty ?? true) {
            onClickValidate()
        }
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let onClickArrowBack = UITapGestureRecognizer(target: self, action: #selector(self.onClickArrowBack(_:)))
        self.uiArrowBack.addGestureRecognizer(onClickArrowBack)
        
        uiPassword.delegate = self
    }
}
