//
//  ParametersParentViewController.swift
//  Learn&Run
//
//  Created by Antonin on 07/07/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class ParametersParentViewController: AbstractController {
    
    @IBOutlet weak var uiArrowBack: UIImageView!
    @IBOutlet weak var uiDelete: UIImageView!
    
    @IBOutlet weak var uiLastName: UITextField!
    @IBOutlet weak var uiFirstName: UITextField!
    @IBOutlet weak var uiPseudo: UITextField!
    
    private func checkIfUserIsFullyAuthenticated(onSuccess: @escaping () -> Void,
                                                 onRetry: @escaping () -> Void) {
        HttpCall.shared.isFullyAuthenticated().call { (isFullyAuthenticatedDto: IsFullyAuthenticatedDto) in
            if isFullyAuthenticatedDto.isFullyAuthenticated {
                onSuccess()
            }
            else {
                // go ask the password because you are not fully authenticated
                let alert = UIAlertController(title: "Entrez votre mot de passe !", message: "Vous devez vous identifier de nouveeau !", preferredStyle: .alert)
                
                let okButton = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    if let password = alert.textFields![0].text {
                        HttpCall.shared.login(username: UserDefaults.standard.string(forKey: "Username")!, password: password).call {
                            onRetry()
                        } onError: { (_) in
                            self.displayAlert(title: "Erreur !", message: "Mot de passe incorrect")
                        }
                    }
                })
                okButton.isEnabled = false
                
                alert.addCustomAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
                alert.addCustomAction(okButton)
                
                alert.addTextField { (textField) in
                    textField.isSecureTextEntry = true
                    NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (_) in
                        let textCount = textField.text?.count ?? 0
                        okButton.isEnabled = textCount > 0
                    }
                }
                
                alert.setCustomStyle()
                self.present(alert, animated: true, completion: nil)
            }
        } onError: { (_) in
            self.displayAlert(title: "Erreur !", message: "Une erreur est survenue")
        }
    }
    
    @IBAction func onClickValidate() {
        checkIfUserIsFullyAuthenticated {
            HttpCall.shared.updateParent(pseudo: self.uiPseudo.text!, firstname: self.uiFirstName.text!, lastname: self.uiLastName.text!).call {
                self.performSegue(withIdentifier: "ParametersToHomeParentSegue", sender: self)
            } onError: { (_) in
                self.displayAlert(title: "Erreur !", message: "Une erreur est survenue")
            }
        } onRetry: {
            self.onClickValidate()
        }
    }
    
    @IBAction func onClickChangeEmail() {
        checkIfUserIsFullyAuthenticated {
            self.performSegue(withIdentifier: "ParametersParentToChangeEmailSegue", sender: self)
        } onRetry: {
            self.onClickChangeEmail()
        }
    }
    
    @IBAction func onClickChangePassword() {
        checkIfUserIsFullyAuthenticated {
            self.performSegue(withIdentifier: "ParametersParentToChangePasswordSegue", sender: self)
        } onRetry: {
            self.onClickChangePassword()
        }
    }
    
    @IBAction func onClickLogout() {
        HttpCall.shared.logout().call()
        HTTPCookieStorage.shared.cookies?.forEach({ (cookie) in
            HTTPCookieStorage.shared.deleteCookie(cookie)
        })
        self.performSegue(withIdentifier: "ParametersParentToLoginSegue", sender: self)
    }
    
    @objc private func onClickArrowBack(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "ParametersToHomeParentSegue", sender: self)
    }
    
    @objc private func onClickDelete(_ sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Êtes-vous sûr de vouloir supprimer votre compte ?", message: "Cette action est définitive !", preferredStyle: .alert)
        
        alert.addCustomAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
        alert.addCustomAction(UIAlertAction(title: "OK", style: .destructive, handler: { (_) in
            HttpCall.shared.deleteUser().call {
                self.performSegue(withIdentifier: "ParametersParentToLoginSegue", sender: self)
            } onError: { (_) in
                self.displayAlert(title: "Erreur !", message: "Une erreur est survenue !")
            }
        }))
        
        alert.setCustomStyle()
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ParametersParentToChangeEmailSegue":
            (segue.destination as! ChangeEmailViewController).homeSegueName = "ChangeEmailToHomeParentSegue"
            (segue.destination as! ChangeEmailViewController).backSegueName = "ChangeEmailToParametersParentSegue"
        case "ParametersParentToChangePasswordSegue":
            (segue.destination as! ChangePasswordViewController).homeSegueName = "ChangePasswordToHomeParentSegue"
            (segue.destination as! ChangePasswordViewController).backSegueName = "ChangePasswordToParametersParentSegue"
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let onClickArrowBack = UITapGestureRecognizer(target: self, action: #selector(self.onClickArrowBack(_:)))
        self.uiArrowBack.addGestureRecognizer(onClickArrowBack)
        let onClickDelete = UITapGestureRecognizer(target: self, action: #selector(self.onClickDelete(_:)))
        self.uiDelete.addGestureRecognizer(onClickDelete)

        HttpCall.shared.getUserInfo().call { (userInfoDto: UserInfoDto) in
            self.uiLastName.text = userInfoDto.user.lastname
            self.uiFirstName.text = userInfoDto.user.firstname
            self.uiPseudo.text = userInfoDto.user.pseudo
        } onError: { (_) in
            self.displayAlert(title: "Erreur !", message: "Une erreur est survenue")
        }
    }
}
