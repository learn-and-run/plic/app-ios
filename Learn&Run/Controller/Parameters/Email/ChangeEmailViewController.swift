//
//  ChangeEmailViewController.swift
//  Learn&Run
//
//  Created by Antonin on 03/10/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class ChangeEmailViewController: AbstractController, FreezeProtocol, UITextFieldDelegate {

    var homeSegueName: String = ""
    var backSegueName: String = ""
    
    @IBOutlet weak var uiEmail: UITextField!
    @IBOutlet weak var uiValidateButton: UIButton!
    @IBOutlet weak var uiLoadingAnimation: UIActivityIndicatorView!
    @IBOutlet weak var uiArrowBack: UIImageView!
    
    private func disableValidateButton() {
        uiValidateButton.isUserInteractionEnabled = false
        let color: UIColor = UIColor(named: "LightGrayColor")!
        uiValidateButton.backgroundColor = color
        uiValidateButton.borderColor = color
    }

    private func enableValidateButton() {
        uiValidateButton.isUserInteractionEnabled = true
        let color: UIColor = UIColor(named: "BeigeColor")!
        uiValidateButton.backgroundColor = color
        uiValidateButton.borderColor = color
    }
    
    private func handleError(message: String) {
        enableValidateButton()
        cancelFreezeScreen()
        displayAlert(title: "Erreur", message: message)
    }
    
    @IBAction func onChangeEmail() {
        if let email = uiEmail.text, !email.isEmpty {
            enableValidateButton()
        }
        else {
            disableValidateButton()
        }
    }
    
    @IBAction func onClickValidate() {
        startFreezeScreen()

        HttpCall.shared.updateEmail(email: uiEmail.text!).call {
            let alert = UIAlertController(title: "Email envoyé !", message: "Vous devez valider votre nouvelle adresse", preferredStyle: .alert)
            
            alert.addCustomAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                self.performSegue(withIdentifier: self.homeSegueName, sender: self)
            }))
            
            alert.setCustomStyle()
            self.present(alert, animated: true, completion: nil)
        } onError: { (_) in
            self.handleError(message: "Une erreur est survenue !")
        }
    }
    
    @objc private func onClickArrowBack(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: backSegueName, sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !(uiEmail.text?.isEmpty ?? true) {
            onClickValidate()
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let onClickArrowBack = UITapGestureRecognizer(target: self, action: #selector(self.onClickArrowBack(_:)))
        self.uiArrowBack.addGestureRecognizer(onClickArrowBack)
        
        uiEmail.delegate = self
    }
}
