//
//  ParametersStudentViewController.swift
//  Learn&Run
//
//  Created by Antonin on 07/07/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import UIKit

class ParametersStudentViewController: AbstractController, UITableViewDelegate, UITableViewDataSource {
    
    private var currentClass: LevelType!
    
    @IBOutlet weak var uiArrowBack: UIImageView!
    @IBOutlet weak var uiDelete: UIImageView!
    
    @IBOutlet weak var uiLastName: UITextField!
    @IBOutlet weak var uiFirstName: UITextField!
    @IBOutlet weak var uiPseudo: UITextField!
    @IBOutlet weak var uiClass: UITextField!
    @IBOutlet weak var uiClassArrow: UIImageView!
    @IBOutlet weak var uiClassTable: UITableView!
    @IBOutlet weak var uiClassView: UIView!
    @IBOutlet var uiClassTableHeight: NSLayoutConstraint!
    
    private func checkIfUserIsFullyAuthenticated(onSuccess: @escaping () -> Void,
                                                 onRetry: @escaping () -> Void) {
        HttpCall.shared.isFullyAuthenticated().call { (isFullyAuthenticatedDto: IsFullyAuthenticatedDto) in
            if isFullyAuthenticatedDto.isFullyAuthenticated {
                onSuccess()
            }
            else {
                // go ask the password because you are not fully authenticated
                let alert = UIAlertController(title: "Entrez votre mot de passe !", message: "Vous devez vous identifier de nouveeau !", preferredStyle: .alert)
                
                let okButton = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    if let password = alert.textFields![0].text {
                        HttpCall.shared.login(username: UserDefaults.standard.string(forKey: "Username")!, password: password).call {
                            onRetry()
                        } onError: { (_) in
                            self.displayAlert(title: "Erreur !", message: "Mot de passe incorrect")
                        }
                    }
                })
                okButton.isEnabled = false
                
                alert.addCustomAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
                alert.addCustomAction(okButton)
                
                alert.addTextField { (textField) in
                    textField.isSecureTextEntry = true
                    NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (_) in
                        let textCount = textField.text?.count ?? 0
                        okButton.isEnabled = textCount > 0
                    }
                }
                
                alert.setCustomStyle()
                self.present(alert, animated: true, completion: nil)
            }
        } onError: { (_) in
            self.displayAlert(title: "Erreur !", message: "Une erreur est survenue")
        }
    }
    
    @IBAction func onClickValidate() {
        checkIfUserIsFullyAuthenticated {
            HttpCall.shared.updateStudent(pseudo: self.uiPseudo.text!, firstname: self.uiFirstName.text!, lastname: self.uiLastName.text!, levelType: self.currentClass).call {
                self.performSegue(withIdentifier: "ParametersToHomeStudentSegue", sender: self)
            } onError: { (_) in
                self.displayAlert(title: "Erreur !", message: "Une erreur est survenue")
            }
        } onRetry: {
            self.onClickValidate()
        }
    }
    
    @IBAction func onClickChangeEmail() {
        checkIfUserIsFullyAuthenticated {
            self.performSegue(withIdentifier: "ParametersStudentToChangeEmailSegue", sender: self)
        } onRetry: {
            self.onClickChangeEmail()
        }
    }
    
    @IBAction func onClickPassword() {
        checkIfUserIsFullyAuthenticated {
            self.performSegue(withIdentifier: "ParametersStudentToChangePasswordSegue", sender: self)
        } onRetry: {
            self.onClickPassword()
        }
    }
    
    @IBAction func onClickLogout() {
        HttpCall.shared.logout().call()
        HTTPCookieStorage.shared.cookies?.forEach({ (cookie) in
            HTTPCookieStorage.shared.deleteCookie(cookie)
        })
        self.performSegue(withIdentifier: "ParametersStudentToLoginSegue", sender: self)
    }
    
    private func openDropDown() {
        view.endEditing(true)
        
        UIView.animate(withDuration: 0.3) {
            self.uiClassTableHeight.isActive = false
            
            self.uiClassView.backgroundColor = UIColor(named: "BeigeColor")
            self.uiClassView.borderColor = UIColor(named: "BeigeColor")
            self.uiClass.textColor = UIColor(named: "GreenColor")
            self.uiClassArrow.tintColor = UIColor(named: "GreenColor")
            
            self.uiClassArrow.transform = CGAffineTransform(rotationAngle: .pi)
            self.view.layoutIfNeeded()
            self.uiClassTable.isHidden = false
        }
    }
    
    private func hideDropDown() {
        UIView.animate(withDuration: 0.3, animations: {
            self.uiClassTableHeight.isActive = true
            
            self.uiClassView.backgroundColor = nil
            self.uiClassView.borderColor = .black
            self.uiClass.textColor = .black
            self.uiClassArrow.tintColor = .black
            
            self.uiClassArrow.transform = CGAffineTransform(rotationAngle: 0)
            self.view.layoutIfNeeded()
        }) { (_) in
            self.uiClassTable.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        LevelType.allValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = uiClassTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LabelCell
        cell.layoutMargins = UIEdgeInsets.zero
        cell.uiLabel.text = LevelType.allValues[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentClass = LevelType.allValues[indexPath.row]
        uiClass.text = currentClass.name
        hideDropDown()
    }
    
    @objc private func onClickClass(_ sender: UITapGestureRecognizer) {
        if uiClassTable.isHidden {
            openDropDown()
        }
        else {
            hideDropDown()
        }
    }
    
    @objc private func onClickArrowBack(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "ParametersToHomeStudentSegue", sender: self)
    }
    
    @objc private func onClickDelete(_ sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Êtes-vous sûr de vouloir supprimer votre compte ?", message: "Cette action est définitive !", preferredStyle: .alert)
        
        alert.addCustomAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
        alert.addCustomAction(UIAlertAction(title: "OK", style: .destructive, handler: { (_) in
            HttpCall.shared.deleteUser().call {
                self.performSegue(withIdentifier: "ParametersStudentToLoginSegue", sender: self)
            } onError: { (_) in
                self.displayAlert(title: "Erreur !", message: "Une erreur est survenue !")
            }
        }))
        
        alert.setCustomStyle()
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ParametersStudentToChangeEmailSegue":
            (segue.destination as! ChangeEmailViewController).homeSegueName = "ChangeEmailToHomeStudentSegue"
            (segue.destination as! ChangeEmailViewController).backSegueName = "ChangeEmailToParametersStudentSegue"
        case "ParametersStudentToChangePasswordSegue":
            (segue.destination as! ChangePasswordViewController).homeSegueName = "ChangePasswordToHomeStudentSegue"
            (segue.destination as! ChangePasswordViewController).backSegueName = "ChangePasswordToParametersStudentSegue"
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        uiClassTable.delegate = self
        uiClassTable.dataSource = self
        
        let onClickArrowBack = UITapGestureRecognizer(target: self, action: #selector(self.onClickArrowBack(_:)))
        self.uiArrowBack.addGestureRecognizer(onClickArrowBack)
        let onClickDelete = UITapGestureRecognizer(target: self, action: #selector(self.onClickDelete(_:)))
        self.uiDelete.addGestureRecognizer(onClickDelete)
        let onClickClass = UITapGestureRecognizer(target: self, action: #selector(self.onClickClass(_:)))
        self.uiClassView.addGestureRecognizer(onClickClass)
        
        uiClassTable.layoutMargins = UIEdgeInsets.zero
        uiClassTable.separatorInset = UIEdgeInsets.zero
        
        HttpCall.shared.getUserInfo().call { (userInfoDto: UserInfoDto) in
            self.uiLastName.text = userInfoDto.user.lastname
            self.uiFirstName.text = userInfoDto.user.firstname
            self.uiPseudo.text = userInfoDto.user.pseudo
            self.currentClass = (userInfoDto.user as! StudentDto).levelType
            self.uiClass.text = self.currentClass.name
        } onError: { (_) in
            self.displayAlert(title: "Erreur !", message: "Une erreur est survenue")
        }
    }
}
