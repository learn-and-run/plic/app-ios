//
//  UpdateParentDto.swift
//  Learn&Run
//
//  Created by Antonin on 12/09/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

class UpdateParentDto: Codable {
    
    let pseudo: String
    let firstname: String
    let lastname: String
}
