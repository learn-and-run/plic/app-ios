//
//  IsAuthenticatedDto.swift
//  Learn&Run
//
//  Created by Antonin on 12/09/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

class IsAuthenticatedDto: Codable {
    
    let isAuthenticated: Bool
}
