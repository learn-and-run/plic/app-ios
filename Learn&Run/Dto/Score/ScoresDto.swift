//
//  ScoreDto.swift
//  Learn&Run
//
//  Created by Antonin on 03/07/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

class ScoresDto: Codable {
    
    let scores: [ScoreDto]
    
    class ScoreDto: Codable {
        
        let circuitName: String
        let moduleType: ModuleType
        let score: Int
        let spendTime: Int
        let bestTime: Int
    }
}
