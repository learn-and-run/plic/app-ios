//
//  RankingDto.swift
//  Learn&Run
//
//  Created by Antonin on 17/11/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

class RankingDto: Codable {
    
    let friends: [FriendDto]
    let moduleType: ModuleType
    
    class FriendDto: Codable {
        
        let ladder: Int
        let pseudo: String
        let average: Int
        let circuits: [CircuitDto]
        
        class CircuitDto: Codable {
            
            let name: String
            let bestScore: Int
            let spendTime: Int
            let bestTime: Int
        }
    }
}
