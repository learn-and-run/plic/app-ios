//
//  UserInfoDto.swift
//  Learn&Run
//
//  Created by Antonin on 12/09/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

class UserInfoDto: Codable {
    
    let userType: UserType
    let user: UserDto

    private enum CodingKeys: String, CodingKey {
        case userType
        case user
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        userType = try container.decode(UserType.self, forKey: .userType)

        switch userType {
        case .STUDENT:
            user = try container.decode(StudentDto.self, forKey: .user)
        case .PARENT:
            user = try container.decode(ParentDto.self, forKey: .user)
        case .PROFESSOR:
            user = try container.decode(ProfessorDto.self, forKey: .user)
        }
    }
}

class UserDto: Codable {
    
    let pseudo: String
    let firstname: String
    let lastname: String
    let email: String
    let userType: UserType
    let character: CharacterDto

    class CharacterDto: Codable {
        
        let id: Int
        let name: String
    }
}

class StudentDto: UserDto {
    
    let levelType: LevelType

    private enum CodingKeys: String, CodingKey {
        case levelType
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        levelType = try container.decode(LevelType.self, forKey: .levelType)

        try super.init(from: decoder)
    }
}

class ParentDto: UserDto {
}

class ProfessorDto: UserDto{
}
