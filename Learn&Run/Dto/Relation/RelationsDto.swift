//
//  RelationsDto.swift
//  Learn&Run
//
//  Created by Antonin on 03/07/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

class RelationsDto: Codable {

    let relations: [RelationDto]

    class RelationDto: Codable {

        let id: Int
        let user: UserDto
        let userType: UserType

        private enum CodingKeys: String, CodingKey {
            case id
            case user
            case userType
        }

        required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            id = try container.decode(Int.self, forKey: .id)
            userType = try container.decode(UserType.self, forKey: .userType)

            switch userType {
            case .STUDENT:
                user = try container.decode(StudentDto.self, forKey: .user)
            case .PARENT:
                user = try container.decode(ParentDto.self, forKey: .user)
            case .PROFESSOR:
                user = try container.decode(ProfessorDto.self, forKey: .user)
            }
        }

        class UserDto: Codable {

            let pseudo: String
            let firstname: String
            let lastname: String
            let email: String
            let userType: UserType
            let character: CharacterDto

            class CharacterDto: Codable {

                let id: Int
                let name: String

                init(id: Int, name: String) {
                    self.id = id
                    self.name = name
                }
            }
        }

        class StudentDto: UserDto {

            let levelType: LevelType

            private enum CodingKeys: String, CodingKey {
                case levelType
            }

            required init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)

                levelType = try container.decode(LevelType.self, forKey: .levelType)

                try super.init(from: decoder)
            }
        }

        class ParentDto: UserDto {
        }

        class ProfessorDto: UserDto{
        }
    }
}
