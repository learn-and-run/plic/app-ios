//
//  PendingInvitationsDto.swift
//  Learn&Run
//
//  Created by Antonin on 03/10/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

class PendingInvitationsDto: Codable {
    
    let pendingInvitations: [PendingInvitationDto]
    
    class PendingInvitationDto: Codable {
        
        let id: Int
        let user: UserInvitationDto
        let userType: UserType
        
        class UserInvitationDto: Codable {
            
            let pseudo: String
            let firstname: String
            let lastname: String
            let email: String
            let character: CharacterInvitationDto
            
            class CharacterInvitationDto: Codable {
                
                let id: Int
                let name: String
            }
        }
    }
}
