//
//  LevelType.swift
//  Learn&Run
//
//  Created by Antonin on 11/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

enum LevelType: String, Codable {
    
    case SIXIEME
    case CINQUIEME
    case QUATRIEME
    case TROISIEME
    
    var name: String {
        switch self {
        case .SIXIEME:
            return "Sixième"
        case .CINQUIEME:
            return "Cinquième"
        case .QUATRIEME:
            return "Quatrième"
        case .TROISIEME:
            return "Troisième"
        }
    }
    
    static let allValues = [SIXIEME, CINQUIEME, QUATRIEME, TROISIEME]
}
