//
//  ModuleType.swift
//  Learn&Run
//
//  Created by Antonin on 15/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import SwiftUI

enum ModuleType: String, Codable {
    
    case GENERAL
    case MATHS
    case FRANCAIS
    case PHYSIQUE
    case CHIMIE
    case ANGLAIS
    case HISTOIRE
    case GEOGRAPHIE
    
    var name: String {
        switch self {
        case .GENERAL:
            return "Général"
        case .MATHS:
            return "Mathématiques"
        case .FRANCAIS:
            return "Français"
        case .PHYSIQUE:
            return "Physique"
        case .CHIMIE:
            return "Chimie"
        case .ANGLAIS:
            return "Anglais"
        case .HISTOIRE:
            return "Histoire"
        case .GEOGRAPHIE:
            return "Géographie"
        }
    }
    
    var color: UIColor {
        switch self {
        case .GENERAL:
            return UIColor(red: 41/255, green: 47/255, blue: 51/255, alpha: 1)
        case .MATHS:
            return UIColor(red: 81/255, green: 196/255, blue: 127/255, alpha: 1)
        case .FRANCAIS:
            return UIColor(red: 28/255, green: 124/255, blue: 172/255, alpha: 1)
        case .PHYSIQUE:
            return UIColor(red: 211/255, green: 127/255, blue: 80/255, alpha: 1)
        case .CHIMIE:
            return UIColor(red: 219/255, green: 152/255, blue: 61/255, alpha: 1)
        case .ANGLAIS:
            return UIColor(red: 205/255, green: 68/255, blue: 75/255, alpha: 1)
        case .HISTOIRE:
            return UIColor(red: 206/255, green: 128/255, blue: 178/255, alpha: 1)
        case .GEOGRAPHIE:
            return UIColor(red: 165/255, green: 97/255, blue: 158/255, alpha: 1)
        }
    }

    static let allValues = [GENERAL, MATHS, FRANCAIS, PHYSIQUE, CHIMIE, ANGLAIS, HISTOIRE, GEOGRAPHIE]
}
