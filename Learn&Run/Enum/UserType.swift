//
//  UserType.swift
//  Learn&Run
//
//  Created by Antonin on 11/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

enum UserType: String, Codable {

    case STUDENT
    case PARENT
    case PROFESSOR
}
