//
// Created by Antonin on 12/06/2020.
// Copyright (c) 2020 Ryubi. All rights reserved.
//

import Alamofire

extension HttpCall {

    func login(username: String, password: String) -> DataRequest {
        let parameters = ["username": username, "password": password, "remember-me": "true"]
        return AF.request(url + "login", method: .post, parameters: parameters)
    }

    func logout() -> DataRequest {
        AF.request(url + "logout", method: .delete)
    }

    func isAuthenticated() -> DataRequest {
        AF.request(url + "user/is_auth")
    }

    func isFullyAuthenticated() -> DataRequest {
        AF.request(url + "user/is_fully_auth")
    }
}
