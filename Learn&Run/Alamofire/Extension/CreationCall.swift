//
// Created by Antonin on 12/06/2020.
// Copyright (c) 2020 Ryubi. All rights reserved.
//

import Alamofire

extension HttpCall {

    func createStudent(pseudo: String, firstname: String, lastname: String, password: String, email: String, levelType: LevelType) -> DataRequest {
        let parameters = [
            "pseudo": pseudo,
            "firstname": firstname,
            "lastname": lastname,
            "password": password,
            "email": email,
            "levelType": "\(levelType)"
        ]
        return AF.request(url + "user/create_student", method: .post, parameters: parameters, encoding: JSONEncoding.default)
    }

    func createParent(pseudo: String, firstname: String, lastname: String, password: String, email: String) -> DataRequest {
        let parameters = [
            "pseudo": pseudo,
            "firstname": firstname,
            "lastname": lastname,
            "password": password,
            "email": email
        ]
        return AF.request(url + "user/create_parent", method: .post, parameters: parameters, encoding: JSONEncoding.default)
    }
}
