//
// Created by Antonin on 12/06/2020.
// Copyright (c) 2020 Ryubi. All rights reserved.
//

import Alamofire

extension HttpCall {

    func getUserInfo() -> DataRequest {
        AF.request(url + "user/info", method: .get)
    }
}
