//
//  ScoreCall.swift
//  Learn&Run
//
//  Created by Antonin on 15/06/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import Alamofire

extension HttpCall {

    func getScoresByPseudoAndModule(pseudo: String, moduleType: ModuleType) -> DataRequest {
        let parameters = [
            "pseudo": pseudo,
            "moduleType": "\(moduleType)"
        ]
        return AF.request(url + "score/module", method: .get, parameters: parameters, encoding: URLEncoding.default)
    }
    
    func getFriendsScoresByModule(moduleType: ModuleType) -> DataRequest {
        let parameters = [
            "moduleType": "\(moduleType)"
        ]
        return AF.request(url + "score/friends", method: .get, parameters: parameters, encoding: URLEncoding.default)
    }
}
