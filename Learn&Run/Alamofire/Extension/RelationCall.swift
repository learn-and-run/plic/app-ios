//
//  RelationCall.swift
//  Learn&Run
//
//  Created by Antonin on 03/07/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import Alamofire

extension HttpCall {

    func sendInvitation(username: String) -> DataRequest {
        let parameters = [
            "username": username
        ]
        return AF.request(url + "user/relation/add", method: .post, parameters: parameters, encoding: JSONEncoding.default)
    }
    
    func getPendingInvitations() -> DataRequest {
        AF.request(url + "user/relation/pending", method: .get)
    }
    
    func acceptInvitation(id: Int) -> DataRequest {
        let parameters = [
            "id": id
        ]
        return AF.request(url + "user/relation/validate", method: .post, parameters: parameters, encoding: JSONEncoding.default)
    }
    
    func declineInvitation(id: Int) -> DataRequest {
        AF.request(url + "user/relation/decline/\(id)", method: .delete)
    }
    
    private func getRelations(userType: UserType?) -> DataRequest {
        let parameters: Parameters? = userType != nil ? ["userType": "\(userType!)"] : nil
        
        return AF.request(url + "user/relation/all", method: .get, parameters: parameters, encoding: URLEncoding.default)
    }
    
    func getAllParents() -> DataRequest {
        getRelations(userType: .PARENT)
    }
    
    func getAllStudents() -> DataRequest {
        getRelations(userType: .STUDENT)
    }
    
    func deleteRelation(id: Int) -> DataRequest {
        AF.request(url + "user/relation/delete/\(id)", method: .delete)
    }
}
