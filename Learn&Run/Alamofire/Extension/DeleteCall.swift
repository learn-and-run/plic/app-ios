//
// Created by Antonin on 12/06/2020.
// Copyright (c) 2020 Ryubi. All rights reserved.
//

import Alamofire

extension HttpCall {

    func deleteUser() -> DataRequest {
        AF.request(url + "user/delete", method: .delete)
    }
}
