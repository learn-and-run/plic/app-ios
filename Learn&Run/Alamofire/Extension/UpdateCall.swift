//
// Created by Antonin on 12/06/2020.
// Copyright (c) 2020 Ryubi. All rights reserved.
//

import Alamofire

extension HttpCall {

    func updateStudent(pseudo: String, firstname: String, lastname: String, levelType: LevelType) -> DataRequest {
        let parameters = [
            "pseudo": pseudo,
            "firstname": firstname,
            "lastname": lastname,
            "levelType": "\(levelType)"
        ]
        return AF.request(url + "user/update/student", method: .patch, parameters: parameters, encoding: JSONEncoding.default)
    }

    func updateParent(pseudo: String, firstname: String, lastname: String) -> DataRequest {
        let parameters = [
            "pseudo": pseudo,
            "firstname": firstname,
            "lastname": lastname
        ]
        return AF.request(url + "user/update/parent", method: .patch, parameters: parameters, encoding: JSONEncoding.default)
    }

    func updateEmail(email: String) -> DataRequest {
        let parameters = [
            "email": email
        ]
        return AF.request(url + "user/update/email", method: .patch, parameters: parameters, encoding: JSONEncoding.default)
    }

    func updatePassword(password: String) -> DataRequest{
        let parameters = [
            "password": password
        ]
        return AF.request(url + "user/update/password", method: .patch, parameters: parameters, encoding: JSONEncoding.default)
    }

    func askResetPassword(email: String) -> DataRequest {
        let parameters = [
            "email": email
        ]
        return AF.request(url + "user/reset", method: .post, parameters: parameters, encoding: JSONEncoding.default)
    }
}
