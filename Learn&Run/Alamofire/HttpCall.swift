//
//  HttpCall.swift
//  Learn&Run
//
//  Created by Antonin on 12/04/2020.
//  Copyright © 2020 Ryubi. All rights reserved.
//

import Alamofire

class HttpCall {
    
    private static var instance: HttpCall? = nil
    
    public static var shared: HttpCall {
        if instance == nil {
            instance = HttpCall()
        }
        return instance!
    }
    
    internal let url = "https://back-end-learn-run.herokuapp.com/api/"
}

extension DataRequest {

    func call<T: Decodable>(onSuccess: ((T) -> Void)? = nil,
                            onError: ((DataResponse<Any, AFError>) -> Void)? = nil) {
        self.responseJSON { (response) in
            if let statusCode = response.response?.statusCode {
                if 200 ... 299 ~= statusCode {
                    if let value = try? JSONDecoder().decode(T.self, from: response.data!) {
                        onSuccess?(value)
                        return
                    }
                }
            }
            onError?(response)
        }
    }

    func call(onSuccess: (() -> Void)? = nil,
              onError: ((DataResponse<Any, AFError>) -> Void)? = nil) {
        self.responseJSON { (response) in
            if let statusCode = response.response?.statusCode {
                if 200 ... 299 ~= statusCode {
                    onSuccess?()
                    return
                }
            }
            onError?(response)
        }
    }
}
